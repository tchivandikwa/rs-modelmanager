﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO; // for DirectoryInfo
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace RevenueScience.ModelManager
{
    using RevenueScience.ModelManager.ModelManagement;
    using RevenueScience.ModelManager.SqlManagement;

    public partial class MainForm : Form
    {
        public MainForm()
        {
            this.InitializeComponent();
        }

        // button1 = Select folder where models are stored.
        private void ButtonDirClick(object sender, EventArgs e)
        {
            // Select the desired folder and this populates it to "filepath" as a string.
            if (this.folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.pathTextBox.Text = this.folderBrowserDialog.SelectedPath;
                this.buttonCalculate.Enabled = true;
            }
        }

        private void ButtonCalculateClick(object sender, EventArgs e)
        {
            // Set the cursor to spinning orb (Wait)
            Cursor.Current = Cursors.WaitCursor;

            // Find the models to process
            if (string.IsNullOrEmpty(this.pathTextBox.Text) || Directory.Exists(this.pathTextBox.Text))
            {
                MessageBox.Show("Invalid model path selected.", "Model Manager Status", MessageBoxButtons.OK);
                return;
            }

            var modelsToProcess = Directory.GetFiles(this.pathTextBox.Text, "*.cm", SearchOption.TopDirectoryOnly);

            // Set up the progress bar
            this.progressBar.Maximum = modelsToProcess.Length * 2;
            this.progressBar.Value = 0;

            try
            {
                // Open the SQL conenction.
                using (var sqlManager = new SqlManager(@"Data Source=.\SQLEXPRESS;Initial Catalog=MULTICHOICE;Integrated Security=SSPI"))
                {
                    // Clearing (truncate) the tables
                    sqlManager.TruncateTable("[Multichoice].[dbo].ICValues");
                    sqlManager.TruncateTable("[Multichoice].[dbo].Reports");
                }
            }
            catch (Exception ex)
            {
                var message = string.Format("An error occured while executing SQL: \r\n{0}", ex.Message);
                MessageBox.Show(message, "Model Manager Status", MessageBoxButtons.OK);
                return;
            }

            foreach (var filename in modelsToProcess)
            {
                try
                {
                    var modelManager = new ModelManager(filename);
                    var scenario = modelManager.CreateScenario("MM", "AutoCalc");

                    // Import data from consolidated model
                    modelManager.RunAdaptorV2Process("IMPORT 1");
                    scenario.RecalculateEntireModel();

                    // Export Intercompany Data
                    modelManager.RunAdaptorV2Process("EXPORT 1");
                    modelManager.DeleteScenario(scenario);
                }
                catch (Exception ex)
                {
                    var message = string.Format("An error occured processing '{0}': \r\n{1}", filename, ex.Message);
                    MessageBox.Show(message, "Model Manager Status", MessageBoxButtons.OK);
                    return;
                }

                this.progressBar.PerformStep();
            }

            foreach (var filename in modelsToProcess)
            {
                try
                {
                    var modelManager = new ModelManager(filename);
                    var scenario = modelManager.CreateScenario("MM", "AutoCalc");

                    // Import data from consolidated model
                    modelManager.RunAdaptorV2Process("IMPORT 1");
                    // Import Intercompany Data
                    modelManager.RunAdaptorV2Process("IMPORT 2");
                    scenario.RecalculateEntireModel();

                    // Export Final Results (Reports)
                    modelManager.RunAdaptorV2Process("EXPORT 2");
                    modelManager.DeleteScenario(scenario);
                }
                catch (Exception ex)
                {
                    var message = string.Format("An error occured processing '{0}': \r\n{1}", filename, ex.Message);
                    MessageBox.Show(message, "Model Manager Status", MessageBoxButtons.OK);
                    return;
                }


                this.progressBar.PerformStep();
            }

            // Set the cursor back to default
            Cursor.Current = Cursors.Default;

            // Completion Message Box
            MessageBox.Show("Model files have been calculated successfully.\nNew data pushed to database and is ready to be imported.", "Model Manager Status", MessageBoxButtons.OK);
        }

        private void ButtonExitClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}


