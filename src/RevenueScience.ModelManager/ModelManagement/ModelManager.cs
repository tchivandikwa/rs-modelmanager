﻿// -----------------------------------------------------------------------
// <copyright file="ModelManager.cs" company="Cyest Corporation">
//     Copyright (c) Cyest Corporation 2015. All rights reserved.
// </copyright>
// <author>R. Smith</author>
// -----------------------------------------------------------------------

namespace RevenueScience.ModelManager.ModelManagement
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Cyest.Carbon.Modeller.Server.Models;
    using Cyest.Carbon.Modeller.Server.Models.Scenarios;

    /// <summary>
    /// Wrapper around a <see cref="Model"/> which provides model management helper functions.
    /// </summary>
    public class ModelManager
    {
        private readonly Model model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelManager"/> class.
        /// </summary>
        /// <param name="fileName">Name of the model file to load.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="fileName"/> is null.</exception>
        public ModelManager(string fileName)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }

            if (!File.Exists(fileName))
            {
                throw new ArgumentException("The specified file does not exist: " + fileName, "fileName");
            }

            this.model = new Model(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelManager"/> class.
        /// </summary>
        /// <param name="model">The model to wrap.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="model"/> is null.</exception>
        public ModelManager(Model model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            this.model = model;
        }

        /// <summary>
        /// Creates a scenario in the model.
        /// </summary>
        /// <param name="name">The name of the scenario.</param>
        /// <param name="description">The description of the scenario.</param>
        /// <returns>The new scenario.</returns>
        /// <exception cref="System.ArgumentNullException">The <paramref name="name"/> or <paramref name="description"/> is null.</exception>
        public IScenarioDataSet CreateScenario(string name, string description)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            if (description == null)
            {
                throw new ArgumentNullException("description");
            }

            var id = this.model.ScenarioManager.CreateScenario(name, description, true);

            return this.model.ScenarioManager.LoadDataSet(id);
        }

        /// <summary>
        /// Deletes a scenario in the model.
        /// </summary>
        /// <param name="scenarioDataSet">The scenario to delete.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="scenarioDataSet"/> is null.</exception>
        public void DeleteScenario(IScenarioDataSet scenarioDataSet)
        {
            if (scenarioDataSet == null)
            {
                throw new ArgumentNullException("scenarioDataSet");
            }

            this.model.ScenarioManager.DeleteScenario(scenarioDataSet.ScenarioId);
        }

        /// <summary>
        /// Runs an AdaptorV2 process.
        /// </summary>
        /// <param name="processName">Name of the process to run.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="processName"/> is null.</exception>
        public void RunAdaptorV2Process(string processName)
        {
            if (processName == null)
            {
                throw new ArgumentNullException("processName");
            }

            this.model.AdapterV2Manager().RunAdapterV2Process(processName);
        }
    }
}
