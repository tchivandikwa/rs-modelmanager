﻿// -----------------------------------------------------------------------
// <copyright file="SqlManager.cs" company="Cyest Corporation">
//     Copyright (c) Cyest Corporation 2015. All rights reserved.
// </copyright>
// <author>R. Smith</author>
// -----------------------------------------------------------------------

namespace RevenueScience.ModelManager.SqlManagement
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Helper class for SQL actions.
    /// </summary>
    public class SqlManager : IDisposable
    {
        private readonly SqlConnection connection;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlManager"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="connectionString"/> is null.</exception>
        public SqlManager(string connectionString)
        {
            if (connectionString == null)
            {
                throw new ArgumentNullException("connectionString");
            }

            this.connection = new SqlConnection(connectionString);
            this.connection.Open();
        }

        /// <summary>
        /// Truncates a table.
        /// </summary>
        /// <param name="tableName">Name of the table to truncate.</param>
        public void TruncateTable(string tableName)
        {
            string sanitizedName;

            using (var cmd = new SqlCommand("select quotename(@tableName)", this.connection))
            {
                cmd.Parameters.Add(new SqlParameter("@tableName", tableName));
                sanitizedName = (string)cmd.ExecuteScalar();
            }

            using (var cmd = new SqlCommand("TRUNCATE TABLE " + sanitizedName, this.connection))
            {
                cmd.ExecuteNonQuery();
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.connection.Dispose();
        }

        #endregion
    }
}
